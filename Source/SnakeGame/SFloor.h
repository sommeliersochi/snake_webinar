﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ADeadWall.h"
#include "Portal.h"
#include "SFloorElement.h"
#include "GameFramework/Actor.h"
#include "SFloor.generated.h"

class AFood;
class UHierarchicalInstancedStaticMeshComponent;


UCLASS()
class SNAKEGAME_API ASFloor : public AActor
{
	GENERATED_BODY()

public:
	ASFloor();

protected:
	virtual void BeginPlay() override;

	void GenerateFloor();
	void GenerateWallsAndPortals();
	void GenerateСircleWalls();

	UPROPERTY(/*EditAnywhere, Category= "Map Settings"*/)
	int MapSize = 5;

	UPROPERTY()
	int MapDivision = 5;

	UPROPERTY()
	float CircleCenter = 6;

	UPROPERTY()
	float CircleSize = 12;

	UPROPERTY()
	float PortalDivision = 5;

	TArray<FVector> AllSegmentLocations;

	UPROPERTY(EditDefaultsOnly, Category= "Map Settings|Visual")
	TArray<UMaterialInterface*> ElementMaterials;
	
	UPROPERTY(EditDefaultsOnly, Category= "Food")
	TSubclassOf<AFood> FoodClass;

	/*UPROPERTY(EditDefaultsOnly, Category= "Component")
	UHierarchicalInstancedStaticMeshComponent* HierarchicalInstancedFloor;*/

	/*UPROPERTY(EditDefaultsOnly, Category= "Component")
	UHierarchicalInstancedStaticMeshComponent* HierarchicalInstancedWall;*/

	UPROPERTY(EditDefaultsOnly, Category= "Wall Components|PortalBP")
	TSubclassOf<APortal> PortalClass;

	UPROPERTY(EditDefaultsOnly, Category= "Wall Components|WallBP")
	TSubclassOf<AADeadWall> WallClass;

	UPROPERTY(EditDefaultsOnly, Category= "Wall Components|FloorBP")
	TSubclassOf<ASFloorElement> FloorClass;

	UPROPERTY()
	APortal* Portal;

	UPROPERTY()
	AADeadWall* Wall;
	
	UPROPERTY()
	ASFloorElement* Floor;
	
	UPROPERTY()
	AFood* FoodRef;

	UFUNCTION()
	FVector GetRandomFoodPos();

	void ChangeFoodPos();

	float X;

	float Y;

	float MapCenter;

public:
	//virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;

	void AddFood();
};
