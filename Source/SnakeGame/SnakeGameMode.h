// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/GameModeBase.h"
#include "SnakeGameMode.generated.h"

class USSaveGame;
/**
 * 
 */
UCLASS()
class SNAKEGAME_API ASnakeGameMode : public AGameModeBase
{
	GENERATED_BODY()
protected:

	UPROPERTY()
	FString SlotName;
	
	UPROPERTY()
	USSaveGame* CurrentSaveGame;

	
public:
	
	UPROPERTY(BlueprintReadWrite)
	bool IsMenu = true;

	ASnakeGameMode();
	
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;

	void HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer) override;

	UFUNCTION(BlueprintCallable, Category="SaveGame")
	void WriteSaveGame();

	void LoadSaveGame();
	
};
