// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WidgetGameOver.h"
#include "SFloor.h"
#include "GameFramework/Pawn.h"
#include "Sound/SoundCue.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	
	APlayerPawnBase();

protected:

	virtual void BeginPlay() override;
	

public:

	void CreateGameOverWidget();
	
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();
	
	void CreateFloorActor();

	void PlayMoveSound(FVector Location) const;
	void PlayEatSound(FVector Location) const;
	void PlayDiedSound(FVector Location) const;
	void PlayPortalSound(FVector Location) const;
	
	UFUNCTION()
	void RestartGame();

	UFUNCTION()
		void HandlePlayerVerticalInput(float value);
	
	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);

protected:

	float LostSpeed =0.0;

	UPROPERTY(EditDefaultsOnly, Category= "Sound")
	USoundCue* MoveSound;

	UPROPERTY(EditDefaultsOnly, Category= "Sound")
	USoundCue* EatSound;

	UPROPERTY(EditDefaultsOnly, Category= "Sound")
	USoundCue* DiedSound;

	UPROPERTY(EditDefaultsOnly, Category= "Sound")
	USoundCue* PortalSound;
	
	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(BlueprintReadWrite)
	ASFloor* FloorActor;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASFloor> FloorActorClass;

	UPROPERTY(BlueprintReadWrite)
	UWidgetGameOver* GameOverWidget;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UWidgetGameOver> GameOverWidgetClass;

};
