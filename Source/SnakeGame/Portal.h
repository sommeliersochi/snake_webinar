﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "MagicNumberTypes.h"
#include "GameFramework/Actor.h"
#include "Portal.generated.h"

class UHierarchicalInstancedStaticMeshComponent;
UCLASS()
class SNAKEGAME_API APortal : public AActor, public IInteractable
{
	GENERATED_BODY()

public:
	APortal();

protected:

	virtual void BeginPlay() override;

public:

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	void SetPortalDistance(float Value);

	float GetPortalDistance();

protected:
	
	UPROPERTY(EditDefaultsOnly, Category= "Component")
	UStaticMeshComponent* StaticMeshComp;
	
	float PortalDistance = ELEMENT_SIZE;

};
