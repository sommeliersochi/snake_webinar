// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SSaveGame.h"
#include "Engine/GameInstance.h"
#include "SGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API USGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	virtual void Init() override;
	
	UFUNCTION(BlueprintNativeEvent)
	void LoadGameInstance(USSaveGame* SaveObject);
	
	UFUNCTION(BlueprintCallable)
	void SetMapSize(int NewMapSize);

	int GetMapSize() const { return MapSize; }


protected:

	int MapSize = 15;
	
};
