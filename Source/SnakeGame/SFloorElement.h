﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SFloorElement.generated.h"

UCLASS()
class SNAKEGAME_API ASFloorElement : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASFloorElement();
protected:

	UPROPERTY(EditDefaultsOnly, Category= "Component")
	UStaticMeshComponent* StaticMeshComp;

public:

	FORCEINLINE UStaticMeshComponent* GetStaticMeshComp() { return StaticMeshComp; }
};
