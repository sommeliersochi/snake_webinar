// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "PlayerPawnBase.h"
#include "Food.generated.h"

class ASnakeBase;
class ASFloor;
UCLASS()
class SNAKEGAME_API AFood : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	AFood();

protected:

	void EatFood(ASnakeBase* SnakeBase, float SpeedBonus, int AddElement);

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite, Category= "Component")
	UStaticMeshComponent* StaticMeshComp;

	UPROPERTY()
	ASFloor* FloorRef;

public:	
	
	virtual void Interact(AActor* Interactor, bool bIsHead) override;
	
	void SetFloorRef(ASFloor* Floor) { FloorRef = Floor; }
	

};
