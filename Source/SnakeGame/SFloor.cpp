﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "SFloor.h"

#include "ADeadWall.h"
#include "DrawDebugHelpers.h"
#include "Food.h"
#include "MagicNumberTypes.h"
#include "SGameInstance.h"
#include "SnakeElementBase.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "Kismet/KismetMathLibrary.h"

ASFloor::ASFloor()
{
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");
	
	/*HierarchicalInstancedFloor = CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>("HierarchicalInstancedFloor");
	HierarchicalInstancedFloor->SetupAttachment(RootComponent);*/

	/*HierarchicalInstancedWall = CreateDefaultSubobject<UHierarchicalInstancedStaticMeshComponent>("HierarchicalInstancedWall");
	HierarchicalInstancedWall->SetupAttachment(HierarchicalInstancedFloor);*/

	//MapSize = 5;
		
}

void ASFloor::BeginPlay()
{
	Super::BeginPlay();

	if(USGameInstance* GI = GetWorld()->GetGameInstance<USGameInstance>())
	{
		MapSize = GI->GetMapSize();
	}

	GenerateFloor();
	GenerateWallsAndPortals();
	GenerateСircleWalls();
	
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParameters.Instigator = this->GetInstigator();
	
	FoodRef = GetWorld()->SpawnActor<AFood>(
		FoodClass,
		GetRandomFoodPos(),
		FRotator::ZeroRotator,
		SpawnParameters
		);
	
	FoodRef->SetFloorRef(this);
	
}

void ASFloor::GenerateСircleWalls()
{
	FVector L_Scale = FVector(1, 1, 1);
	FVector L_Location;

	int L_MapSize = MapSize+ 2;

	int L_MapCenter = (L_MapSize / 2 * ELEMENT_SIZE) /*+ ELEMENT_SIZE * 4*/;

	for (int i = 0; i < L_MapSize; i++)
	{
		Y = i * ELEMENT_SIZE - L_MapCenter;

		for (int j = 0; j < L_MapSize; j++)
		{
			X = j * ELEMENT_SIZE - L_MapCenter;
			
			L_Location = UKismetMathLibrary::MakeVector(X + ELEMENT_SIZE, Y + ELEMENT_SIZE, 0.f);

			if (MapSize == 15)
			{
				if ( i == 3 && j == 5 || i == 3 && j == 9 ||
				i == 4 && j == 5 || i == 4 && j == 9 ||
				
				i == 5 && j == 3 || i == 5 && j == 4 || i == 5 && j == 5 ||
				i == 5 && j == 9 ||	i == 5 && j == 10 || i == 5 && j == 11 ||

				i == 7 && j == 7 ||

				i == 9 && j == 3 || i == 9 && j == 4 ||	i == 9 && j == 5 ||
				i == 9 && j == 9 || i == 9 && j == 10 || i == 9 && j == 11 ||
				
				i == 10 && j == 5 || i == 10 && j == 9 ||
				i == 11 && j == 5 || i == 11 && j == 9)
				{
					Wall = GetWorld()->SpawnActor<AADeadWall>(WallClass, L_Location, FRotator::ZeroRotator);
				}
			}
			if (MapSize == 11)
			{
				if ( i == 3 && j == 2 || i == 3 && j == 3 || i == 3 && j == 4 || i == 3 && j == 5 || i == 3 && j == 6 || i == 3 && j == 7 || i == 3 && j == 8 ||
					i == 7 && j == 2 || i == 7 && j == 3 || i == 7 && j == 4 || i == 7 && j == 5 || i == 7 && j == 6 || i == 7 && j == 7 || i == 7 && j == 8)
				{
					Wall = GetWorld()->SpawnActor<AADeadWall>(WallClass, L_Location, FRotator::ZeroRotator);
				}
			}
			
		}
	}
}


void ASFloor::GenerateWallsAndPortals()
{
	//HierarchicalInstancedWall->ClearInstances();

	FVector L_Scale = FVector(1, 1, 1);
	FVector L_Location;

	int L_MapSize = MapSize + 2;

	int L_MapCenter = (L_MapSize / 2 * ELEMENT_SIZE) + ELEMENT_SIZE /** 2*/;

	for (int i = 0; i < L_MapSize; i++)
	{
		Y = i * ELEMENT_SIZE - L_MapCenter;

		for (int j = 0; j < L_MapSize; j++)
		{
			X = j * ELEMENT_SIZE - L_MapCenter;
			
			L_Location = UKismetMathLibrary::MakeVector(X + ELEMENT_SIZE, Y + ELEMENT_SIZE, 0.f);

			if ((i >= 1 && i <= L_MapSize - 2) && (j >= 1 && j <= L_MapSize - 2))
			{
				//@comment: 
			}

			else
			{
				FTransform SpawnTransform = UKismetMathLibrary::MakeTransform(
					L_Location,
					FRotator::ZeroRotator,
					L_Scale
					);
				
				//Wall = GetWorld()->SpawnActor<AADeadWall>(WallClass, L_Location, FRotator::ZeroRotator);								// Wall Spawn Action
				Portal = GetWorld()->SpawnActor<APortal>(PortalClass,L_Location,FRotator::ZeroRotator/*,SpawnParameters*/);
				

				/*if (i == (MapSize + 2) / 2 || j == (MapSize + 2) / 2)
				{					
					Wall->Destroy();
					
					FActorSpawnParameters SpawnParameters;
					SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
					
					Portal = GetWorld()->SpawnActor<APortal>(
						PortalClass,
						L_Location,
						FRotator::ZeroRotator,
						SpawnParameters
						);*/

					if (Portal)
					{
						Portal->SetPortalDistance(L_MapSize);
					}
				/*}*/
			}
		}
	}
}

void ASFloor::GenerateFloor()
{
	AllSegmentLocations.Empty();

	//HierarchicalInstancedFloor->ClearInstances();

	FVector L_Scale = FVector(1.01, 1.01, 1.01);
	FVector L_Location = FVector::ZeroVector;
	
	int L_MapCenter = (MapSize / 2 * ELEMENT_SIZE) + ELEMENT_SIZE;
	MapCenter = L_MapCenter;

	for (int i = 1; i <= MapSize; i++)
	{
		Y = i * ELEMENT_SIZE - L_MapCenter;

		for (int j = 1; j <= MapSize; j++)
		{
			X = j * ELEMENT_SIZE - L_MapCenter;

			L_Location = UKismetMathLibrary::MakeVector(X, Y, -ELEMENT_SIZE);

			FTransform SpawnTransform = UKismetMathLibrary::MakeTransform(
				L_Location,
				FRotator::ZeroRotator,
				L_Scale
				);

			
			//HierarchicalInstancedFloor->AddInstanceWorldSpace(SpawnTransform);
			TArray<UStaticMeshComponent*> FloorMeshes;
			Floor = GetWorld()->SpawnActor<ASFloorElement>(FloorClass, SpawnTransform);			
			FloorMeshes.Add(Floor->GetStaticMeshComp());
			
			for (auto FloorElement : FloorMeshes)
			{
				int32 RandI = FMath::RandRange(0,2);
				if (IsValid(ElementMaterials[RandI]) && IsValid(FloorElement))
				{
					FloorElement->SetMaterial(0,ElementMaterials[RandI]);
				}					
			}		

			AllSegmentLocations.Add(L_Location);
		}
	}
}

void ASFloor::AddFood()
{
	if (!FoodRef)
	{
		return;
	}

	FTimerHandle SpawnFoodHandl;
	GetWorldTimerManager().SetTimer(SpawnFoodHandl,this, &ASFloor::ChangeFoodPos, 1, false);
	
	
}

FVector ASFloor::GetRandomFoodPos()
{
	FVector FoodLocations;

	bool BlockItem = false;

	int32 Count = 0;

	do
	{
		FoodLocations = AllSegmentLocations[FMath::RandRange(0, AllSegmentLocations.Num() - 1)];

		FHitResult HitResult;
		FVector TraceStart = FoodLocations - FVector(0.0f, 0.0f, 10.f);
		FVector TraceEnd = FoodLocations * FVector(0.0f, 0.0f, -150.f);

		GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECC_Visibility);
		DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Yellow, false, 3.f, 0, 12.f);
		
		if (const auto SnakeActor = Cast<ASnakeElementBase>(HitResult.GetActor()))
		{
			DrawDebugSphere(GetWorld(), HitResult.GetActor()->GetActorLocation(), 10.f, 24, FColor::Green, false, 5.0f);
			BlockItem = true;
		}

		BlockItem = HitResult.bBlockingHit;

		Count++;
	}
	while (BlockItem && Count < 10);

	return FoodLocations;
}

void ASFloor::ChangeFoodPos()
{
	FoodRef->SetActorLocation(GetRandomFoodPos());
	FoodRef->SetActorHiddenInGame(false);
}

/*void ASFloor::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	GenerateFloor(); //TODO:: ala Constructor 
}*/


