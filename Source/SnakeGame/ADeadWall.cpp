﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ADeadWall.h"

#include "PlayerPawnBase.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"


AADeadWall::AADeadWall()
{
	PrimaryActorTick.bCanEverTick = false;
	
	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>("MeshComp");
	RootComponent = StaticMeshComp;
	float Scale = 0.95f;
	StaticMeshComp->SetRelativeScale3D(FVector(Scale, Scale, Scale));
}

void AADeadWall::Interact(AActor* Interactor, bool bIsHead)
{
	IInteractable::Interact(Interactor, bIsHead);

	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		{			
			Snake->Destroy();
		}
	}
}

