// Fill out your copyright notice in the Description page of Project Settings.


#include "WidgetGameOver.h"

#include "SnakeGameMode.h"
#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"

void UWidgetGameOver::Setup()
{
	this->AddToViewport();
	GetGameMode()->WriteSaveGame();
	
	UWorld* World = GetWorld();

	PlayerController = World->GetFirstPlayerController();

	FInputModeGameAndUI InputModeData;
	InputModeData.SetWidgetToFocus(this->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::LockOnCapture);
	InputModeData.SetHideCursorDuringCapture(false);

	PlayerController->SetInputMode(InputModeData);
	PlayerController->bShowMouseCursor = true;

}

void UWidgetGameOver::RestartButtonClicked()
{
	FName CurrentLevelName = FName(UGameplayStatics::GetCurrentLevelName(GetWorld()));
	UGameplayStatics::OpenLevel(GetWorld(), CurrentLevelName, true);
}

void UWidgetGameOver::ExitButtonClicked()
{
	UKismetSystemLibrary::QuitGame(GetWorld(), PlayerController, EQuitPreference::Quit, true);
}

ASnakeGameMode* UWidgetGameOver::GetGameMode()
{
	ASnakeGameMode* GameMode = GetWorld()->GetAuthGameMode<ASnakeGameMode>();
	
	return GameMode;
}

void UWidgetGameOver::NativeConstruct()
{
	Super::NativeConstruct();

	if (RestartButton && ExitButton)
	{
		RestartButton->OnClicked.AddDynamic(this, &UWidgetGameOver::RestartButtonClicked);
		ExitButton->OnClicked.AddDynamic(this, &UWidgetGameOver::UWidgetGameOver::ExitButtonClicked);
	}
}

